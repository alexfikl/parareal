// SPDX-FileCopyrightText: 2014 Alexandru Fikl <alexfikl@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "utils.h"
#include "workspace.h"

int main(int argc, char **argv) {
    MPI_Comm mpicomm = MPI_COMM_WORLD;

    workspace_t *w = NULL;
    double time = 0.0;

    /* init MPI */
    MPI_Init(&argc, &argv);
    utils_init(mpicomm);

    /* create workspace */
    w = workspace_new(mpicomm, argc, argv);

    /* solve the ODE */
    time = (double) clock();
    workspace_solve(w);
    time = ((double) clock() - time) / CLOCKS_PER_SEC;
    MPI_Allreduce(MPI_IN_PLACE, &time, 1, MPI_DOUBLE, MPI_SUM, mpicomm);
    LOG_GLOBAL("[LOG] Elapsed time: %g / %g", time / w->mpisize, time);

    /* save to file */
    workspace_save(w);

    /* destroy workspace */
    workspace_destroy(w);

    /* finalize MPI */
    utils_finalize();
    MPI_Finalize();

    return 0;
}

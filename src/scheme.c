// SPDX-FileCopyrightText: 2014 Alexandru Fikl <alexfikl@gmail.com>
//
// SPDX-License-Identifier: MIT

#include "scheme.h"
#include "utils.h"

#ifndef NEWTON_DY
#define NEWTON_DY 1e-8
#endif

#ifndef NEWTON_NMAX
#define NEWTON_NMAX 100L
#endif

#ifndef NEWTON_EPS
#define NEWTON_EPS 1e-12
#endif

typedef double (*newton_function_t)(
    function_t f, double t, double y, double dt, double yn
);

static double derivative(function_t f, double t, double y) {
    return (f(t, y + NEWTON_DY) - f(t, y)) / NEWTON_DY;
}

static double
newton_be_function(function_t f, double t, double y, double dt, double yn) {
    return yn + dt * f(t, y) - y;
}

static double
newton_be_derivative(function_t f, double t, double y, double dt, double yn) {
    UNUSED(yn);

    return dt * derivative(f, t, y) - 1;
}

static double
newton_cn_function(function_t f, double t, double y, double dt, double yn) {
    return yn + 0.5 * dt * (f(t - dt, yn) + f(t, y)) - y;
}

static double
newton_cn_derivative(function_t f, double t, double y, double dt, double yn) {
    UNUSED(yn);

    return 0.5 * dt * derivative(f, t, y) - 1;
}

static double newton_rhapson(
    newton_function_t f,
    newton_function_t df,
    function_t fy,
    double t,
    double dt,
    double yn
) {
    size_t n = 0;
    double error = 1.1 * NEWTON_EPS;

    double fpy = 0;
    double yopt = yn;
    double yold = yn;

    while (n < NEWTON_NMAX && error > NEWTON_EPS) {
        /* compute and check the derivative */
        fpy = df(fy, t, yopt, dt, yn);
        CHECK_ABORT(!FUZZYZERO(fpy), "Derivative at %g is 0.", yopt);

        /* update our value */
        yopt = yopt - f(fy, t, yopt, dt, yn) / fpy;

        /* compute the error */
        error = fabs(yopt - yold);
        yold = yopt;

        /* next iteration */
        ++n;
    }
    //     debug ("Convergence: %lu / %lu its\t error %g", n, NEWTON_NMAX, error);

    return yopt;
}

double ode_integrate_be(function_t f, double tn, double yn, double dt) {
    return newton_rhapson(newton_be_function, newton_be_derivative, f, tn, dt, yn);
}

double ode_integrate_cn(function_t f, double tn, double yn, double dt) {
    return newton_rhapson(newton_cn_function, newton_cn_derivative, f, tn, dt, yn);
}

double ode_integrate_fe(function_t f, double tn, double yn, double dt) {
    return yn + dt * f(tn, yn);
}

double ode_integrate_rk2(function_t f, double tn, double yn, double dt) {
    double k1 = dt * f(tn, yn);
    double k2 = dt * f(tn + 0.5 * dt, yn + 0.5 * k1);

    return yn + k2;
}

void ode(
    integrator_t integrate, function_t f, double *y, size_t n, double t0, double dt
) {
    for (size_t i = 0; i < (n - 1); ++i) {
        y[i + 1] = integrate(f, t0 + i * dt, y[i], dt);
    }
}

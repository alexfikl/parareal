// SPDX-FileCopyrightText: 2014 Alexandru Fikl <alexfikl@gmail.com>
//
// SPDX-License-Identifier: MIT

#include "utils.h"

static int utils_identifier = -1;
static int utils_malloc_count = 0;

void utils_init(MPI_Comm mpicomm) {
    MPI_Comm_rank(mpicomm, &utils_identifier);
    utils_malloc_count = 0;
}

void utils_finalize(void) {
    if (utils_malloc_count != 0) {
        LOG_GLOBAL("[WARN] Uneven malloc / free calls: %d.", utils_malloc_count);
    }
}

void *utils_malloc(size_t size) {
    void *p = malloc(size);
    CHECK_ABORT(p != NULL, "Out of memory");

    ++utils_malloc_count;
    return p;
}

void *utils_calloc(size_t n, size_t size) {
    void *p = calloc(n, size);
    CHECK_ABORT(p != NULL, "Out of memory");

    ++utils_malloc_count;
    return p;
}

char *utils_strdup(const char *str) {
    char *p = utils_malloc(strlen(str) + 1);
    strcpy(p, str);

    return p;
}

void utils_free(void *p) {
    --utils_malloc_count;
    free(p);
}

int utils_is_root(void) {
    return utils_identifier <= 0;
}

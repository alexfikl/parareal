// SPDX-FileCopyrightText: 2014 Alexandru Fikl <alexfikl@gmail.com>
//
// SPDX-License-Identifier: MIT

/**
 * \file parareal.h
 * \brief Implementation of the Parareal algorithm and associated data structs.
 *
 * This files contains the functions to use to solve an ODE in parallel using
 * the Parareal algorithm and MPI.
 */
#ifndef __PARAREAL_H__
#define __PARAREAL_H__

#include <mpi.h>
#include <stdlib.h>

#include "scheme.h"

/**
 * \struct parareal_data_t
 * \brief Struct describing the data necessary for the Parareal algorithm.
 */
typedef struct {
    MPI_Comm mpicomm; /**< MPI Communicator. */
    int mpisize;      /**< Number of processes. */
    int mpirank;      /**< ID of current process. */

    function_t f; /**< Right hand side */
    double t0;    /**< Global starting time. */
    double y0;    /**< Initial condition for \f$t = t_0\f$ */

    double N;  /**< Points for coarse operator. */
    double *Y; /**< Coarse variables. */

    double n;  /**< Points for fine operator. */
    double *y; /**< Fine variables. */
} parareal_data_t;

/**
 * \brief Allocate the struct and set the initial condition.
 *
 * Available initial condition ids are:
 *
 * <table>
 * <tr>
 *  <td>Id</td>
 *  <td>Right-hand side</td>
 *  <td>Initial condition</td>
 * </tr>
 *
 * <tr>
 *  <td>exp</td>
 *  <td>\f$ -y \f$</td>
 *  <td>\f$ \exp(-t)\f$</td>
 * </tr>
 *
 * <tr>
 *  <td>cos_sin</td>
 *  <td>\f$ 2 \cos^2(t) \sin(t) - \frac{1}{\cos(t)} - \tan(t) y \f$</td>
 *  <td>\f$ -0.5 \cos(t) \cos(2 t) - \sin(t) + 7 \cos(t) \f$</td>
 * </tr>
 *
 * </table>
 *
 * If the id is \c NULL, the two fields for the initial condition are left alone
 * and it is the user's job to set them before calling parareal().
 *
 * \param[in] id        The id of the initial condition.
 * \param[in] N         The number of local iterations for the coarse solver.
 * \param[in] n         The number of local iterations for the fine solver.
 * \param[in] t0        Global starting time.
 * \param[in] mpicomm   MPI Communicator.
 *
 * \return A fully allocated struct.
 */
parareal_data_t *
parareal_data_new(const char *id, size_t N, size_t n, double t0, MPI_Comm mpicomm);

/**
 * \brief Free the data.
 */
void parareal_data_destroy(parareal_data_t *data);

/**
 * \brief Perform the Parareal iterations.
 *
 * The stopping condition for the Parareal algorithm is based on two
 * conditions. The first one involves a maximum number of iterations \a kmax.
 * The second one includes an error computed as:
 * \f[
 *      e_k = \sum_{p = 0}^P |Y^{k}_{N} - Y^{k - 1}_N|
 * \f]
 * that is the sum over all processes of the absolute value obtained on the
 * coarse grid at the end of the current interval.
 *
 * The basic idea of the Parareal algorithm is to separate a time interval
 * \f$ [t_0, t_n] \f$ into P subintervals (where P is the number of processes).
 * Each subinterval is then treated separately with the iteration working on
 * the initial condition of each interval.
 *
 * For an ODE, if we know the initial condition, we can solve it completely.
 * The Parareal algorithm operates on a coarse and a fine grid on each
 * subinterval as follows:
 *
 *  * sequentially initialize the coarse grid on all processes.
 *  * start iteration for k
 *  * parallelly compute a fine solution on each subinterval using the results
 *  from the coarse grid.
 *  * send solution at the end of each subinterval to the next process
 *  * use the initial condition received from the previous interval as another
 *  initial condition for a coarse resolution.
 *  * correct the initial condition in the current subinterval as follows:
 * \f[
 *      Y^k_n = Y^k_{n - 1} + y^{k - 1}_{n - 1} - Y^{k - 1}_{n - 1},
 * \f]
 *  where \f$ Y \f$ is the coarse solution and \f$ y \f$ is the fine solution.
 *  * the iteration then continues with \f$ Y^k_n \f$ as an initial solution
 *  for the fine grid.
 *
 * \param[in,out] data      The info for the Parareal algorithm.
 * \param[in] dT            Coarse time step.
 * \param[in] dt            Fine time step.
 * \param[in] kmax          Maximum number of Parareal iterations.
 */
void parareal(parareal_data_t *data, double dt, double dT, size_t kmax);

#endif /* __PARAREAL_H__ */

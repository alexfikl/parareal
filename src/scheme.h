// SPDX-FileCopyrightText: 2014 Alexandru Fikl <alexfikl@gmail.com>
//
// SPDX-License-Identifier: MIT

/**
 * \file scheme.h
 * \brief Classic, sequential ODE schemes.
 *
 * This file contains several classic implicit and explicit ODE schemes:
 *
 *  * Forward and backward Euler (first-order explicit and implicit).
 *  * Crank-Nicolson (second-order implicit).
 *  * Runge Kutta (second-order explicit).
 */
#ifndef __SCHEME_H__
#define __SCHEME_H__

#include <stdlib.h>

/**
 * \brief Prototype for the right hand side.
 */
typedef double (*function_t)(double t, double y);

/**
 * \brief Integrating function.
 */
typedef double (*integrator_t)(function_t f, double tn, double yn, double dt);

/**
 * \brief The implicit Euler method for integrating ODEs.
 *
 * This function is used to integrate:
 * \f[
 *      y'(t) = f(t, y(t))
 * \f]
 * by using the implicit Euler method, given by:
 * \f[
 *      y_{n + 1} = y_n + \Delta t\ f(t_{n + 1}, y_{n + 1})
 * \f]
 * To find the new value of \f$ y_{n + 1} \f$, we use the Newton-Rhapson
 * method with \f$ y_n \f$ as an initial solution.
 *
 * This method is first order accurate.
 *
 * \param[in] f     The right hand side.
 * \param[in] tn    Time at iteration \f$ n \f$.
 * \param[in] yn    Solution at iteration \f$ n \f$.
 * \param[in] dt    Time step.
 *
 * \return The value at iteration \f$ n + 1 \f$.
 */
double ode_integrate_be(function_t f, double tn, double yn, double dt);

/**
 * \brief The semi-implicit Crank-Nicolson method for integrating ODEs.
 *
 * This function is used to integrate:
 * \f[
 *      y'(t) = f(t, y(t))
 * \f]
 * by using the Crank-Nicolson method, given by:
 * \f[
 *      y_{n + 1} = y_n + \frac{\Delta t}{2} (f(t_n, y_n) + f(t_{n + 1}, y_{n + 1}))
 * \f]
 *
 * To find the new value of \f$ y_{n + 1} \f$, we use the Newton-Rhapson method
 * with \f$ y_n \f$ as an initial solution.
 *
 * This method is second order accurate.
 *
 * \param[in] f     The right hand side.
 * \param[in] tn    Time at iteration \f$ n \f$.
 * \param[in] yn    Solution at iteration \f$ n \f$.
 * \param[in] dt    Time step.
 *
 * \return The value at iteration \f$ n + 1 \f$.
 */
double ode_integrate_cn(function_t f, double tn, double yn, double dt);

/**
 * \brief The explicit Euler method for integrating ODEs.
 *
 * This function is used to integrate:
 * \f[
 *      y'(t) = f(t, y(t))
 * \f]
 * by using the explicit Euler method, given by:
 * \f[
 *      y_{n + 1} = y_n + \Delta t\ f(t_n, y_n)
 * \f]
 *
 * This method is first order accurate.
 *
 * \param[in] f     The right hand side.
 * \param[in] tn    Time at iteration \f$ n \f$.
 * \param[in] yn    Solution at iteration \f$ n \f$.
 * \param[in] dt    Time step.
 *
 * \return The value at iteration \f$ n + 1 \f$.
 */
double ode_integrate_fe(function_t f, double tn, double yn, double dt);

/**
 * \brief The explicit second order Runge-Kutta method for integrating ODEs.
 *
 * This function is used to integrate:
 * \f[
 *      y'(t) = f(t, y(t))
 * \f]
 * by using the explicit Runge-Kutta method, given by:
 * \f{eqnarray*}{
 *      k_1 &=& \Delta t\ f(t_n, y_n) \\
 *      k_2 &=& \Delta t\ f(t_n + \Delta t, y_n + k_1) \\
 *      y_{n + 1} &=& y_n + \frac{1}{2} (k_1 + k_2)
 * \f}
 *
 * This method is second order accurate.
 *
 * \param[in] f     The right hand side.
 * \param[in] tn    Time at iteration \f$ n \f$.
 * \param[in] yn    Solution at iteration \f$ n \f$.
 * \param[in] dt    Time step.
 *
 * \return The value at iteration \f$ n + 1 \f$.
 */
double ode_integrate_rk2(function_t f, double tn, double yn, double dt);

/**
 * \brief Integrate an ODE using a given integrator.
 *
 * This function is used to integrate:
 * \f[
 *      y'(t) = f(t, y(t))
 * \f]
 * using a user provided integrator, such as ode_integrate_be() or others
 * that are already included.
 *
 * \param[in] integrate An integrating function.
 * \param[in] f     The right hand side.
 * \param[in,out] y The solution vector for all time steps. The value of y[0]
 *                  has to be filled in with the initial condition. The function
 *                  will fill in all the other time steps.
 * \param[in] n     Number of time steps.
 * \param[in] t0    Starting time.
 * \param[in] dt    Time step.
 */
void ode(
    integrator_t integrate, function_t f, double *y, size_t n, double t0, double dt
);

#endif /* !__SCHEME_H__ */

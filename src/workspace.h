// SPDX-FileCopyrightText: 2014 Alexandru Fikl <alexfikl@gmail.com>
//
// SPDX-License-Identifier: MIT

/**
 * \file workspace.h
 * \brief Global struct describing the simulation.
 *
 * This file contains the general "workspace" for the simulation, which handles
 * and stores all the command line options and deals with IO.
 */
#ifndef __WORKSPACE_H__
#define __WORKSPACE_H__

#include <mpi.h>
#include <stdlib.h>

#include "parareal.h"

/**
 * \struct workspace_t
 * \brief Main structure that contains the state of the simulation.
 */
typedef struct {
    MPI_Comm mpicomm; /**< The used MPI Communicator. */
    int mpisize;      /**< Number of MPI processes. */
    int mpirank;      /**< Rank of the current process. */

    char *filename; /**< Output filename. */
    double t0;      /**< Start of the time interval. */
    double tn;      /**< End of the time interval. */

    double dT; /**< Coarse time step. */
    double dt; /**< Fine time step. */

    size_t N;              /**< Number of points for the coarse grid. */
    size_t n;              /**< Number of points for each fine grid. */
    parareal_data_t *data; /**< Contains the needed variables. */
} workspace_t;

/**
 * \brief Initialize a new workspace.
 *
 * Expects MPI to be initialized.
 *
 * \param[in] mpicomm       MPI Communicator.
 * \param[in] argc          Number of arguments.
 * \param[in] argv          Arguments from main.
 *
 * \return An initialized workspace.
 */
workspace_t *workspace_new(MPI_Comm mpicomm, int argc, char **argv);

/**
 * \brief Free all the allocated data.
 *
 * \param[in] w     The workspace.
 */
void workspace_destroy(workspace_t *w);

/**
 * \brief Compute the solution at all time steps.
 *
 * \param[in] w     The workspace.
 */
void workspace_solve(workspace_t *w);

/**
 * \brief Save the solution to the given file.
 *
 * The solution is saved in pairs \f$(t_n, y(t_n))\f$. The resulting file is a
 * binary file written using MPI IO and can be opened in \c gnuplot, for example,
 * using:
 *
 * \code{.gnuplot}
 * plot "filename.plt" binary format "%lf%lf" with lines;
 * \endcode
 *
 * \param[in] w     The workspace.
 */
void workspace_save(workspace_t *w);

#endif /* __WORKSPACE_H__ */

// SPDX-FileCopyrightText: 2014 Alexandru Fikl <alexfikl@gmail.com>
//
// SPDX-License-Identifier: MIT

#include "parareal.h"
#include "utils.h"

/****************************************************************************
 *                      Initial Conditions
 ****************************************************************************/
static double exp_f(double t, double y) {
    UNUSED(t);
    return -y;
}

static double exp_y(double t) {
    return exp(-t);
}

static double cos_sin_f(double t, double y) {
    return 2 * pow(cos(t), 2) * sin(t) - 1.0 / cos(t) - tan(t) * y;
}

static double cos_sin_y(double t) {
    return (-0.5 * cos(t) * cos(2 * t) - sin(t) + 7 * cos(t));
}

/****************************************************************************
 *                      Public Functions
 ****************************************************************************/
parareal_data_t *
parareal_data_new(const char *id, size_t N, size_t n, double t0, MPI_Comm mpicomm) {
    int mpiret;

    parareal_data_t *data = utils_malloc(sizeof(parareal_data_t));

    data->mpicomm = mpicomm;
    mpiret = MPI_Comm_size(mpicomm, &(data->mpisize));
    CHECK_MPI(mpiret);
    mpiret = MPI_Comm_rank(mpicomm, &(data->mpirank));
    CHECK_MPI(mpiret);

    /* allocate the arrays */
    data->N = N;
    data->Y = utils_malloc(data->N * sizeof(double));
    data->n = n;
    data->y = utils_malloc(data->n * sizeof(double));

    /* initialize */
    data->t0 = t0;
    if (strcmp(id, "exp") == 0) {
        data->f = exp_f;
        data->y0 = exp_y(t0);
    } else if (strcmp(id, "cos_sin") == 0) {
        data->f = cos_sin_f;
        data->y0 = cos_sin_y(t0);
    } else {
        data->f = NULL;
        data->y0 = 0;
    }

    return data;
}

void parareal_data_destroy(parareal_data_t *data) {
    utils_free(data->Y);
    utils_free(data->y);
    utils_free(data);
}

void parareal(parareal_data_t *data, double dt, double dT, size_t kmax) {
    ASSERT(data->f != NULL);

    MPI_Comm comm = data->mpicomm;
    int mpirank = data->mpirank;
    double t0 = data->t0;

    size_t N = data->N;
    double *Y = data->Y;
    size_t n = data->n;
    double *y = data->y;

    double error = 0;
    double Ynext = 0;
    double Yprev = 0;
    double Yk = 0;

    MPI_Status status;
    int mpiret;

    /* get an initial condition for the current process */
    Y[0] = data->y0;
    for (int p = 0; p < mpirank; ++p, t0 += (N - 1) * dT) {
        /* use the coarse operator to solve on [T_{n}, T_{n + 1}] */
        ode(ode_integrate_cn, data->f, Y, N, t0, dT);

        /* update the initial solution for the next interval */
        Y[0] = Y[N - 1];
    }

    /* do a coarse run on the current interval as well to get Y_{N - 1} */
    ode(ode_integrate_cn, data->f, Y, N, t0, dT);
    Yk = Yprev = Y[N - 1];

    for (size_t k = 0; k < kmax; ++k) {
        y[0] = Y[0];

        /* use the fine integrator on the current interval */
        ode(ode_integrate_rk2, data->f, y, n, t0, dt);

        /* get the new initial condition for the current process */
        if (mpirank == 0) {
            Y[0] = data->y0;
        } else {
            mpiret = MPI_Recv(&(Y[0]), 1, MPI_DOUBLE, mpirank - 1, 42, comm, &status);
            CHECK_MPI(mpiret);
        }

        /* use the coarse integrator on the current interval */
        ode(ode_integrate_cn, data->f, Y, N, t0, dT);

        /* correct the value at T_{p + 1} */
        Ynext = Y[N - 1] + y[n - 1] - Yk;

        /* send the new initial condition to the next process */
        if (mpirank != (data->mpisize - 1)) {
            mpiret = MPI_Send(&(Ynext), 1, MPI_DOUBLE, mpirank + 1, 42, comm);
            CHECK_MPI(mpiret);
        }

        error = fabs(Ynext - Yprev);
        MPI_Allreduce(MPI_IN_PLACE, &error, 1, MPI_DOUBLE, MPI_SUM, comm);

        LOG_DEBUG_GLOBAL("[%lu] error: %g", k, error);
        if (error < 1e-7) {
            LOG_DEBUG_GLOBAL("Stopped at iteration %lu / %lu", k + 1, kmax);
            break;
        }

        Yk = Y[N - 1];
        Yprev = Ynext;
    }
}

// SPDX-FileCopyrightText: 2014 Alexandru Fikl <alexfikl@gmail.com>
//
// SPDX-License-Identifier: MIT

#include "workspace.h"

#include <getopt.h>
#include <string.h>

#include "utils.h"

static struct option options[] = {
    {"filename", required_argument, 0, 'f'},
    {"ncoarse", required_argument, 0, 'N'},
    {"nfine", required_argument, 0, 'n'},
    {"tstart", required_argument, 0, 't'},
    {"tend", required_argument, 0, 'T'},
    {"help", no_argument, 0, 'h'},
    {0, 0, 0, 0}
};

static void usage(void) {
    LOG_GLOBAL("Usage: parareal [OPTIONS] [-h].\n");
    LOG_GLOBAL("Options:");
    LOG_GLOBAL("\t-f, --filename,    File name to save solution");
    LOG_GLOBAL("\t-t, --tstart,      Starting time");
    LOG_GLOBAL("\t-T, --tend,        Ending time");
    LOG_GLOBAL("\t-n, --nfine,       Number of fine points in each process");
    LOG_GLOBAL("\t-N, --ncoarse,     Number of coarse points in each process");
    LOG_GLOBAL("\t-h, --help         Display help message\n");
    LOG_GLOBAL("THIS PROGRAM COMES WITH ABSOLUTELY NO WARRANTY.");
}

static void workspace_handle_options(workspace_t *w, int argc, char **argv) {
    int c = 0;
    int option_index = 0;

    /* set default values */
    w->filename = NULL;
    w->t0 = 0;
    w->tn = 1;
    w->N = w->mpisize;
    w->n = 100;

    while (1) {
        c = getopt_long(argc, argv, "f:n:N:t:T:h", options, &option_index);
        if (c == -1) {
            break;
        }

        switch (c) {
        case 'f':
            w->filename = utils_strdup(optarg);
            break;
        case 'N':
            w->N = atol(optarg);
            break;
        case 'n':
            w->n = atol(optarg);
            break;
        case 't':
            w->t0 = atof(optarg);
            break;
        case 'T':
            w->tn = atof(optarg);
            break;
        case 'h':
            usage();
            MPI_Finalize();
            exit(0);
        case '?':
            break;
        default:
            printf("Unrecognized option: %d.\n", c);
        }
    }

    /* if no filename has been given, use a default one */
    if (w->filename == NULL) {
        w->filename = utils_strdup("output.dat");
    }

    /* compute the time steps */
    w->N = (w->N <= 1 ? 2 : w->N);
    w->n = (w->n <= 1 ? 2 : w->n);
    w->dT = (w->tn - w->t0) / ((w->N - 1) * w->mpisize);
    w->dt = (w->tn - w->t0) / ((w->n - 1) * w->mpisize);

    LOG_DEBUG_GLOBAL("processes:  %d", w->mpisize);
    LOG_DEBUG_GLOBAL("filename:   \"%s\"", w->filename);
    LOG_DEBUG_GLOBAL("time:       [%g, %g]", w->t0, w->tn);
    LOG_DEBUG_GLOBAL("            %-10s %-10s", "coarse", "fine");
    LOG_DEBUG_GLOBAL("points:     %-10lu %-10lu", w->N, w->n);
    LOG_DEBUG_GLOBAL("dt:         %-10g %-10g", w->dT, w->dt);
}

workspace_t *workspace_new(MPI_Comm mpicomm, int argc, char **argv) {
    int mpiret;
    workspace_t *w = utils_malloc(sizeof(workspace_t));

    w->mpicomm = mpicomm;
    mpiret = MPI_Comm_size(mpicomm, &(w->mpisize));
    CHECK_MPI(mpiret);
    mpiret = MPI_Comm_rank(mpicomm, &(w->mpirank));
    CHECK_MPI(mpiret);

    workspace_handle_options(w, argc, argv);
    w->data = parareal_data_new("cos_sin", w->N, w->n, w->t0, w->mpicomm);

    return w;
}

void workspace_destroy(workspace_t *w) {
    parareal_data_destroy(w->data);
    utils_free(w->filename);
    utils_free(w);
}

void workspace_solve(workspace_t *w) {
    CHECK_ABORT(w != NULL, "workspace is not initialized.");
    parareal(w->data, w->dt, w->dT, 2 * w->mpisize);
}

void workspace_save(workspace_t *w) {
    MPI_File file;
    MPI_Status status;
    int mpiret;

    double t0 = w->t0 + w->mpirank * (w->N - 1) * w->dT;
    double dt = w->dt;
    size_t data_size = 0;
    double *data = NULL;

    mpiret = MPI_File_open(
        MPI_COMM_WORLD,
        w->filename,
        MPI_MODE_WRONLY | MPI_MODE_CREATE,
        MPI_INFO_NULL,
        &file
    );
    CHECK_MPI(mpiret);

    /* gather the data */
    data_size = 2 * w->data->n * sizeof(double);
    data = utils_malloc(2 * w->data->n * sizeof(double));
    for (size_t i = 0; i < w->data->n; ++i) {
        data[2 * i + 0] = t0 + i * dt;
        data[2 * i + 1] = w->data->y[i];
    }

    /* write it */
    mpiret = MPI_File_write_at_all(
        file, data_size * w->mpirank, data, 2 * w->data->n, MPI_DOUBLE, &status
    );
    CHECK_MPI(mpiret);

    mpiret = MPI_File_close(&file);
    CHECK_MPI(mpiret);

    utils_free(data);
}

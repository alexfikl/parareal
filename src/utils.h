// SPDX-FileCopyrightText: 2014 Alexandru Fikl <alexfikl@gmail.com>
//
// SPDX-License-Identifier: MIT

/**
 * \file utils.h
 * \brief Random utilities used in the other files.
 */
#ifndef __UTILS_H__
#define __UTILS_H__

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <math.h>
#include <mpi.h>

/**
 * \def UNUSED(x)
 * \brief Silence warnings about unused variable.
 */
#define UNUSED(x) ((void) (x)) /**< @hideinitializer */

/**
 * \def FUZZYZERO(x)
 * \brief Check if \c x is zero or very close to zero.
 */
#define FUZZYZERO(x) (fabs(x) < 1e-12) /**< @hideinitializer */

/**
 * \brief Initialize the utils for use with MPI.
 *
 * \param[in] mpicomm   MPI Communicator
 */
void utils_init(MPI_Comm mpicomm);

/**
 * \brief Finalize the utils for use with MPI.
 */
void utils_finalize(void);

/**
 * \brief Malloc the memory and exit on failure.
 *
 * \param[in] size  The size in bytes to allocate.
 *
 * \return An allocated block of size size.
 */
void *utils_malloc(size_t size);

/**
 * \brief Calloc the memory and exit on failure.
 *
 * \param[in] n     The number of elements to allocate.
 * \param[in] size  The size of each element.
 *
 * \return A newly allocate block of memory of size <code>(n * size)</code> in bytes.
 */
void *utils_calloc(size_t n, size_t size);

/**
 * \brief Free.
 *
 * \param[in] p     The start of the memory block to free.
 */
void utils_free(void *p);

/**
 * \brief strdup implementation since it is not part of the C standard.
 *
 * \param[in] str   The string to duplicate.
 *
 * \return A newly allocated string with the same content as str.
 */
char *utils_strdup(const char *str);

/**
 * \brief Check whether the current process is the root (0).
 */
int utils_is_root(void);

/** \cond */
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define __FUNC__ __func__
#define clean_errno() (errno == 0 ? "None" : strerror(errno))

#define LOGC99(M, ...) fprintf(stderr, M "%s\n", __VA_ARGS__)
#define LOG_DEBUGC99(M, ...)                                                           \
    fprintf(                                                                           \
        stderr,                                                                        \
        "DEBUG %s:%s:%d: " M "%s\n",                                                   \
        __FILENAME__,                                                                  \
        __FUNC__,                                                                      \
        __LINE__,                                                                      \
        __VA_ARGS__                                                                    \
    )
#define LOG_ERRORC99(M, ...)                                                           \
    fprintf(                                                                           \
        stderr,                                                                        \
        "[ERROR] (%s:%s:%d: errno: %s) " M "%s\n",                                     \
        __FILENAME__,                                                                  \
        __FUNC__,                                                                      \
        __LINE__,                                                                      \
        clean_errno(),                                                                 \
        __VA_ARGS__                                                                    \
    )
#define LOG_WARNINGC99(M, ...)                                                         \
    fprintf(                                                                           \
        stderr,                                                                        \
        "[WARN] (%s:%s:%d: errno: %s) " M "%s\n",                                      \
        __FILENAME__,                                                                  \
        __FUNC__,                                                                      \
        __LINE__,                                                                      \
        clean_errno(),                                                                 \
        __VA_ARGS__                                                                    \
    )
#define LOG_INFOC99(M, ...)                                                            \
    fprintf(                                                                           \
        stderr,                                                                        \
        "[INFO] (%s:%s:%d) " M "%s\n",                                                 \
        __FILENAME__,                                                                  \
        __FUNC__,                                                                      \
        __LINE__,                                                                      \
        __VA_ARGS__                                                                    \
    )
#define ABORTC99(M, ...)                                                               \
    do {                                                                               \
        LOG_ERRORC99(M, __VA_ARGS__);                                                  \
        exit(EXIT_FAILURE);                                                            \
    } while (0)
/** \endcond */

/**
 * \def LOG()
 * \brief Printf-style output that prints to stderr.
 */
#define LOG(...) LOGC99(__VA_ARGS__, "") /**< @hideinitializer */

/**
 * \def LOG_ERROR()
 * \brief Printf-style log message with appended filename, line etc for errors.
 */
#define LOG_ERROR(...) LOG_ERRORC99(__VA_ARGS__, "") /**< @hideinitializer */

/**
 * \def LOG_WARNING()
 * \brief Printf-style log message for errors warnings.
 */
#define LOG_WARNING(...) LOG_WARNINGC99(__VA_ARGS__, "") /**< @hideinitializer */

/**
 * \def LOG_INFO()
 * \brief Printf-style log message with appended filename, line etc for misc.
 */
#define LOG_INFO(...) LOG_INFOC99(__VA_ARGS__, "") /**< @hideinitializer */

/**
 * \def LOG_GLOBAL()
 * \brief Printf-style log message with appended filename, line etc.
 *
 * Useful when used with MPI to print messages only from the root (0) process.
 */
#define LOG_GLOBAL(...)                                                                \
    do {                                                                               \
        (utils_is_root() ? LOGC99(__VA_ARGS__, "") : 0);                               \
    } while (0) /**< @hideinitializer */

/**
 * \def ABORT()
 * \brief Print an error message and exit.
 */
#define ABORT(...) ABORTC99(__VA_ARGS__, "") /**< @hideinitializer */

/**
 * \def CHECK_ABORT()
 * \brief Abort if a condition is false.
 */
#define CHECK_ABORT(A, ...)                                                            \
    if (!(A)) {                                                                        \
        ABORTC99(__VA_ARGS__, "");                                                     \
    } /**< @hideinitializer */

/**
 * \def CHECK_MEMORY()
 * \brief Abort if the given pointer is \c NULL.
 */
#define CHECK_MEMORY(A)                                                                \
    CHECK_ABORT((A) == NULL, "Out of memory.") /**< @hideinitializer */

/**
 * \def CHECK_MPI()
 * \brief Abort if the returned MPI value is not \c MPI_SUCCESS.
 */
#define CHECK_MPI(A)                                                                   \
    CHECK_ABORT((A) == MPI_SUCCESS, "MPI Error.") /**< @hideinitializer */

#ifdef NDEBUG
#define LOG_DEBUG(...)
#define LOG_DEBUG_GLOBAL(...)
#define ASSERT(A)
#else
/**
 * \def LOG_DEBUG()
 * \brief Print debug messages.
 *
 * Only prints debug messages when \c NDEBUG was not defined.
 */
#define LOG_DEBUG(...) LOG_DEBUGC99(__VA_ARGS__, "") /**< @hideinitializer */

/**
 * \def LOG_DEBUG_GLOBAL()
 * \brief Print debug messages.
 *
 * Same as \c LOG_DEBUG(), but for use with MPI. Only process 0 prints.
 */
#define LOG_DEBUG_GLOBAL(...)                                                          \
    do {                                                                               \
        (utils_is_root() ? LOG_DEBUGC99(__VA_ARGS__, "") : 0);                         \
    } while (0) /**< @hideinitializer */

/**
 * \def ASSERT()
 * \brief Abort with an assertion message if a given condition is not true.
 *
 * Only asserts when \c NDEBUG is not defined.
 */
#define ASSERT(A) CHECK_ABORT(A, "Assertion \"" #A "\"") /**< @hideinitializer */
#endif

#endif /* __UTILS_H__ */

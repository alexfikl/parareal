project(
    'parareal',
    'c',
    default_options: ['c_std=c18', 'warning_level=3', 'buildtype=debug'],
    license: 'MIT',
    version: '0.1',
)

cc = meson.get_compiler('c')
mpi_dep = dependency('mpi', language: 'c', required: true)
math_dep = cc.find_library('m', required: true)

subdir('src')

doxygen = find_program('doxygen', required: false)
if doxygen.found()
    doxyconfig = configuration_data()
    doxyconfig.set('SOURCE_DIR', meson.project_source_root())

    doxyfile = configure_file(
        input: 'Doxyfile.in',
        output: 'Doxyfile',
        configuration: doxyconfig,
        install: false,
    )

    custom_target(
        'docs',
        input: [doxyfile],
        output: '.',
        command: [doxygen, doxyfile],
        install: false,
        build_by_default: true,
    )
else
    warning('Cannot generate documentation: doxygen is not installed.')
endif

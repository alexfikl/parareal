PYTHON := "python -X dev"
BUILDDIR := "build"

_default:
    @just --list

# {{{ formatting

[doc("Reformat all source code")]
format: justfmt mesonfmt clangfmt

[private]
clang_format directory:
    find {{ directory }} -type f -name '*.c' -exec clang-format -i {} \;
    find {{ directory }} -type f -name '*.h' -exec clang-format -i {} \;

[doc("Run clang-format over all source files")]
clangfmt:
    @just clang_format src

[doc("Run meson format over all meson files")]
mesonfmt:
    meson format --inplace --recursive
    @echo -e "\e[1;32mmeson format clean!\e[0m"

[doc("Run just --fmt over the justfiles")]
justfmt:
    just --unstable --fmt
    @echo -e "\e[1;32mjust --fmt clean!\e[0m"

# }}}
# {{{ linting

[doc("Run all linting checks over the source code")]
lint: typos reuse

[doc("Run typos over the source code and documentation")]
typos:
    typos --sort
    @echo -e "\e[1;32mtypos clean!\e[0m"

[doc("Check REUSE license compliance")]
reuse:
    {{ PYTHON }} -m reuse lint

# }}}
# {{{ develop

[doc("Build project in debug mode")]
debug compiler="clang": purge && compile
    CC={{ compiler }} meson setup \
        --buildtype=debug \
        {{ BUILDDIR }}

[doc("Build the project in release mode")]
release compiler="clang": purge && compile
    CC={{ compiler }} meson setup \
        --buildtype=release -Db_lto=true \
        {{ BUILDDIR }}

[doc("Rebuild the project")]
compile:
    meson compile --verbose -C {{ BUILDDIR }}

[doc("Remove all generated build files")]
purge:
    rm -rf {{ BUILDDIR }} tags

# }}}

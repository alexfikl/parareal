# Compilation

This project uses [meson](https://mesonbuild.com/) for its build system.
It can be compiled using
```bash
meson setup -Dbuildtype=debug -Db_sanitize=undefined build
meson compile -C build
```

It requires an MPI implementation, e.g. OpenMPI.

# Running

The program has several command line arguments that can be seen with
```
parareal --help
```

An example run would look like:
```
mpirun -np 4 parareal --ncoarse 10 --nfine 100 --tstart 0 --tend 2
```
which will run the program on **4 MPI processes**. The coarse integrator on each
process will use **10** points and the fine integrator will use **100**. The
integration will be performed on the interval **[0, 2]** as given by **t0**
and **tn**.

Several integrators are implemented (see `scheme.h`):

* `ode_be`: using the implicit first order Euler method.
* `ode_cn`: using the semi-implicit second order Crank-Nicolson method.
* `ode_fe`: using the explicit first order Euler method.
* `ode_rk2`: using the explicit second order Runge-Kutta method.

These can be used in the `parareal` function in `parareal.c`. We recommend using
the implicit methods for the coarse integrators and the explicit methods for
the fine integrators to avoid any stability issues.

# License

MIT
